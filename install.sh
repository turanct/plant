#!/bin/sh

# Set repo dir
REPO="/usr/local/plant"

# Get Working directory
PWD=`pwd`


# Output
echo "--> \033[0;32mCloning the git repo...\033[0m"

# Clone the git repo
git clone https://bitbucket.org/turanct/plant.git $REPO


# Output
echo "--> \033[0;32mGetting dependencies...\033[0m"

# Go to the git repo
cd $REPO

# Install composer.phar file
curl -sS https://getcomposer.org/installer | php

# Install dependencies
php composer.phar install

# Get back to where we once belonged
cd $PWD


# Output
echo "--> \033[0;32mCreating symlink...\033[0m"

# Create symlink
ln -s /usr/local/plant/plant.php /usr/local/bin/plant


# Output
echo "done"
