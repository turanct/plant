#!/usr/bin/env php
<?php
// Require the composer autoloader
require_once(__DIR__ . '/vendor/autoload.php');

// Create new pimple instance
$app = new Pimple();


/**
 * Download method
 */
$app['download'] = $app->protect(function($url, $filename) use ($app) {
	// Get the downloader from the $ENV
	switch ($app['settings.downloader']) {
		// Wget is an option
		case 'wget':
			// Set download command
			$command = 'wget --output-document='.$filename.' --quiet --tries=2 "'.$url.'"';
			break;

		// Curl is the default
		case 'curl':
		default:
			// Set default download command
			$command = 'curl --output '.$filename.' --location --silent --fail --retry 2 "'.$url.'"';
			break;
	}

	// Set prefix and suffix
	// We're changing directories to the /tmp directory, and after downloading
	// we're going back to where we were before we started downloading.
	$prefix = 'PLANT_PWD=`pwd` && cd '.$app['settings.tmpDir'].' && ';
	$suffix = ' && cd $PLANT_PWD';

	// Download the file
	passthru($prefix.$command.$suffix, $status);

	// Check the status, if not int(0), download failes
	if ($status != 0) {
		throw new Exception('Download failed');
	}

	// Return the filename
	return $app['settings.tmpDir'].'/'.$filename;
});


/**
 * Color method
 */
$app['output'] = $app->protect(function($string, $mode = 'normal') use ($app) {
	switch ($mode) {
		case 'welcome':
			echo "\033[1;31m".$string."\033[0m\n";
			break;

		case 'title':
			echo "\n--> \033[0;32m".$string."\033[0m\n";
			break;

		case 'notice':
			echo "\033[0;36m".$string."\033[0m\n";
			break;

		case 'error':
			echo "\033[0;31m".$string."\033[0m\n";
			break;

		case 'normal':
		default:
			echo $string."\n";
			break;
	}
});


/**
 * Init method
 */
$app['init'] = $app->protect(function() use ($app, $argv) {
	// Get environment variables
	$app['settings.tmpDir'] = (getenv('PLANT_TMPDIR') !== false) ? getenv('PLANT_TMPDIR') : '/tmp/plant';
	$app['settings.downloader'] = (getenv('PLANT_DOWNLOADER') !== false) ? getenv('PLANT_DOWNLOADER') : 'curl';
	$app['settings.dryRun'] = (getenv('PLANT_DRYRUN') !== false) ? true : false;
	$app['mode'] = 'install';

	// Create plant tmp directory
	if (getenv('PLANT_TMPDIR') === false && !file_exists('/tmp/plant')) {
		mkdir('/tmp/plant');
	}

	/**
	 * Get command line options
	 */
	$arguments = $argv;
	// Remove the first argument (command name)
	array_shift($arguments);

	// Get the first argument
	$mode = reset($arguments);

	// Does it start with '--'?
	if ($mode !== false && substr($mode, 0, 2) == '--') {
		// Get mode
		if (in_array(substr($mode, 2), array('install', 'remove', 'update', 'check'))) {
			$app['mode'] = substr($mode, 2);
		}

		// Remove first argument (mode)
		array_shift($arguments);
	}

	// Set seeds list
	$app['seeds'] = $arguments;
});


/**
 * Install method
 */
$app['install'] = $app->protect(function() use ($app) {
	// Walk through the seeds
	foreach ($app['seeds'] as $seed) {
		$app['output']('Planting seed "'.$seed.'"', 'title');

		// Create the classname
		$classname = 'Seeds\\'.strtolower($seed);

		// Does the seed exist?
		if (!class_exists($classname)) {
			$app['output']('This seed does not exist, moving on...', 'error');
			continue;
		}

		// Plant seed
		$sprout = new $classname($app);

		// Grow seed
		// Check if the application is already installed
		if (method_exists($sprout, 'checkInstalled')) {
			if ($sprout->checkInstalled() === true) {
				$app['output']('This plant already exists, moving on...', 'error');
				continue;
			}
		}

		// Check if there's a download method
		if (!method_exists($sprout, 'download')) {
			continue;
		}

		$app['output']('Starting download...', 'notice');

		// Download the file
		if ($app['settings.dryRun'] !== true) {
			try {
				$filename = $sprout->download();
			}
			catch (Exception $e) {
				// Download failed, move on
				$app['output']($e->getMessage().', moving on...', 'error');
				continue;
			}
		}
		else {
			$app['output']('Dry-run mode, skipping download.', 'notice');
		}


		// Check if there's an install method
		if (!method_exists($sprout, 'install')) {
			continue;
		}

		$app['output']('Starting installation...', 'notice');

		// Install the application
		if ($app['settings.dryRun'] !== true) {
			$sprout->install($filename);
		}
		else {
			$app['output']('Dry-run mode, skipping installation.', 'notice');
		}

		$app['output']('Installation finished.', 'notice');
	}

	$app['output']('Out of seeds', 'title');
});


/**
 * Remove method
 */
$app['remove'] = $app->protect(function() use ($app) {
	// Approve action?
	$app['output']('Are you sure you want to remove these seeds? (Y/n)', 'notice');
	$answer = stream_get_line(STDIN, 1024, PHP_EOL);

	// Check answer
	if ($answer != 'Y') {
		$app['output']('Aborting...', 'error');
		return;
	}

	// Walk through the seeds
	foreach ($app['seeds'] as $seed) {
		$app['output']('Removing seed "'.$seed.'"', 'title');

		// Create the classname
		$classname = 'Seeds\\'.strtolower($seed);

		// Does the seed exist?
		if (!class_exists($classname)) {
			$app['output']('This seed does not exist, moving on...', 'error');
			continue;
		}

		// Check seed
		$sprout = new $classname($app);

		// Kill seed
		// Check if the application is already installed
		if (method_exists($sprout, 'checkInstalled')) {
			if ($sprout->checkInstalled() !== true) {
				$app['output']('This plant does not exist, moving on...', 'error');
				continue;
			}
		}

		// Check if there's a remove method
		if (!method_exists($sprout, 'remove')) {
			continue;
		}

		$app['output']('Starting removal...', 'notice');

		// remove the application
		if ($app['settings.dryRun'] !== true) {
			$sprout->remove();
		}
		else {
			$app['output']('Dry-run mode, skipping removal.', 'notice');
		}

		$app['output']('Removal finished.', 'notice');
	}

	$app['output']('Out of seeds', 'title');
});


/**
 * Update method
 */
$app['update'] = $app->protect(function() use ($app) {
	// Output
	$app['output']('Updating Plant...', 'title');

	// Run a git pull
	passthru('PLANT_PWD=`pwd` && cd '.dirname(__FILE__).' && git pull origin master && cd $PLANT_PWD');

	// Output
	$app['output']('Update complete', 'title');
});


/**
 * Check method
 */
$app['check'] = $app->protect(function() use ($app) {
	// Output
	$app['output']('Checking Seed links...', 'title');

	// Walk through seeds
	foreach (glob(__DIR__ . '/src/Seeds/*') as $seedFile) {
		// Get the classname
		$seedClass = 'Seeds\\'.basename($seedFile, '.php');

		if (!class_exists($seedClass)) {
			continue;
		}

		// Get an instance of the Seed
		$seed = new $seedClass($app);

		// Output
		$app['output']('Checking seed "'.$seed->name.'"', 'title');

		// Get the downloader from the $ENV
		switch ($app['settings.downloader']) {
			// Wget is an option
			case 'wget':
				// Set check command
				$command = 'wget --spider --server-response "'.$seed->downloadUrl.'" 2>&1 | grep "HTTP/" | awk \'{print $2}\'';
				break;

			// Curl is the default
			case 'curl':
			default:
				// Set default check command
				$command = 'curl -sIL -w "%{http_code}\\n" -o /dev/null "'.$seed->downloadUrl.'"';
				break;
		}

		// Execute the command!
		$status = exec($command);

		// Output
		switch (substr($status, 0, 1)) {
			case '2':
				$app['output']($status.' OK', 'normal');
				break;

			default:
				if (trim($status) == '') {
					$status = 404;
				}

				$app['output']($status, 'error');

				// Get status info
				$dukgo = json_decode(file_get_contents('https://api.duckduckgo.com/?q=http%20status%20'.substr($status, 0, 3).'&format=json'));
				if (trim($dukgo->AbstractText) != '') {
					$app['output']($dukgo->AbstractText, 'normal');
				}

				break;
		}
	}

	// Output
	$app['output']('Check complete', 'title');
});


/**
 * Run method
 */
$app['run'] = $app->protect(function() use ($app) {
	// Plant welcome
	$app['output']('Plant', 'welcome');

	switch ($app['mode']) {
		case 'install':
			$app['install']();
			break;

		case 'remove':
			$app['remove']();
			break;

		case 'update':
			$app['update']();
			break;

		case 'check':
			$app['check']();
			break;

		default:
			$app['output']('Unknown mode.', 'error');
			break;
	}

	// Output
	$app['output']('Cleaning up...', 'title');

	// Remove plant tmp directory
	if (getenv('PLANT_TMPDIR') === false) {
		passthru('rm -r /tmp/plant');
	}
});


/**
 * Run the application
 */
$app['init']();
$app['run']();
