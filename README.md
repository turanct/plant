# Plant
*An software installation helper for Mac OSX, written in Bash and PHP*

**If you think you should use plant, please, think again and use [Homebrew Cask](https://github.com/phinze/homebrew-cask). This is just a fun project of mine.**


## How does Plant work?
Plant automates some default software installation procedures on Mac OSX, e.g.:

1. Download a `.dmg` file
2. Mount the `.dmg` file
3. Copy the `.app` file to your `/Applications` directory

or

1. Download a `.zip` archive
2. Extract a `.pkg` file from the archive
3. Install the `.pkg` file

Every application that you can install through Plant, starts from a *seed* file, describing the download link, application name, installation procedure and other data. When you 'plant' a 'seed', Plant takes the necessary steps to install the *seed*.


## How to intall Plant?
- Open a terminal window
- Run the installer script, this will clone this git repo to `/usr/local/plant`, and symlink the plant php file to `/usr/local/bin/plant`. Alternatively, you can run `curl -sS https://bitbucket.org/turanct/plant/raw/153ffcab79621cc1bfd661c794bd94932fc6d55f/install.sh | sh` to install Plant.
- The `plant` command is now available


## How to use Plant?

### Install an application
- Open a terminal window
- Plant a 'seed' by running `plant {seed}`, e.g. `plant firefox`
- You can plant multiple seeds, e.g. `plant firefox vlc virtualbox`, these will be installed one by one

### Uninstall an application
*coming soon*

- Open a terminal window
- Remove a 'seed' by running `plant --remove {seed}`, e.g. `plant --remove firefox`
- You can remove multiple seeds, e.g. `plant --remove firefox vlc virtualbox`, these will be removed one by one
- You should use **AppTrap** or similar applications if you want to completely remove an app.

### Dry-run
You can use the environment variable `PLANT_DRYRUN` to do a dry-run install or removal simulation of some seeds.

	PLANT_DRYRUN=1 plant firefox

### Select downloader
You can use the environment variable `PLANT_DOWNLOADER` to select the downloader of your choice. Currently supported are cURL and Wget. cURL is default.

	PLANT_DOWNLOADER=wget plant firefox

### Select tmp directory
You can use the environment variable `PLANT_TMPDIR` to set the tmp directory for Plant. `/tmp/plant` is default.

	PLANT_TMPDIR=/path/to/tmp plant firefox


## Seeds

### Help, this seed doesn't work!
1. Open a terminal
2. Type `cd /usr/local/plant`
3. Locate the seed in `src/Seeds`
4. Edit the seed's file to make it work again
5. `git commit` (yes, `/usr/local/plant` is a git repo!)
6. `git push`
7. Send me a pull request

### How to add a seed?
It's easy! Just create a seed file in the `src/Seeds` directory, describing your seed. It should be a php file containing a class that extends one of these classes:

- `Plant\SeedDmgApp` for .dmg files containing a .app file
- `Plant\SeedDmgPkg` for .dmg files containing a .pkg file
- `Plant\SeedPkg` for .pkg files
- `Plant\SeedZipApp` for .zip files containing a .app file
- `Plant\SeedZipPrefPane` for .zip files containing a .prefPane file

*If you need an example, check out some existing Seeds!*


## How to remove plant?
Run these commands in a terminal window:

	rm -f /usr/local/bin/plant
	rm -rf /usr/local/plant


## Disclaimer
Use at your own risk. This application is a fun project, it is not intended for use in production environments, and Unicorns, Orcs, Wookies and other species might appear to hunt you when you run this. I'm not responible for any damage done to you or your belongings.


## License
GPLv3

