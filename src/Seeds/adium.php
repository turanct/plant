<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * adium class
 */
class adium extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'Adium';

	/**
	 * @var string
	 */
	protected $homepage = 'https://adium.im/';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://downloads.sourceforge.net/project/adium/Adium_1.5.7.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'Adium 1.5.7';

	/**
	 * @var string
	 */
	protected $appName = 'Adium.app';
}
