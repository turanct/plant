<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * wop class
 */
class wop extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'World Of Padman';

	/**
	 * @var string
	 */
	protected $homepage = 'http://worldofpadman.net';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://downloads.sourceforge.net/project/worldofpadman/wop-1.5.1-full.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'World of Padman - 1.5.1';

	/**
	 * @var string
	 */
	protected $appName = 'World of Padman - 1.5.1';
}
