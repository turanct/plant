<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * assaultcube class
 */
class assaultcube extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'AssaultCube';

	/**
	 * @var string
	 */
	protected $homepage = 'http://assault.cubers.net';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://downloads.sourceforge.net/project/actiongame/AssaultCube%20Version%201.1.0.4/AssaultCube_v1.1.0.4.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'assaultcube';

	/**
	 * @var string
	 */
	protected $appName = 'AssaultCube.app';
}
