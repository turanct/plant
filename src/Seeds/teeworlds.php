<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * Teeworlds class
 */
class teeworlds extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'Teeworlds';

	/**
	 * @var string
	 */
	protected $homepage = 'https://www.teeworlds.com/';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://teeworlds.com/files/teeworlds-0.6.2-osx.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'Teeworlds';

	/**
	 * @var string
	 */
	protected $appName = 'Teeworlds.app';
}
