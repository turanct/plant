<?php
namespace Seeds;


use Plant\SeedZipApp as Seed;

/**
 * boxer class
 */
class boxer extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'boxer';

	/**
	 * @var string
	 */
	protected $homepage = 'http://boxerapp.com';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://boxerapp.com/download/latest';

	/**
	 * @var string
	 */
	protected $appName = 'Boxer.app';
}
