<?php
namespace Seeds;


use Plant\SeedZipApp as Seed;

/**
 * istumbler class
 */
class istumbler extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'iStumbler';

	/**
	 * @var string
	 */
	protected $homepage = 'http://www.istumbler.net';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://www.istumbler.net/downloads/istumbler-99.zip';

	/**
	 * @var string
	 */
	protected $appName = 'iStumbler.app';
}
