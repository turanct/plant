<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * zeroad class
 */
class zeroad extends Seed {
	/**
	 * @var string
	 */
	protected $name = '0 A.D.';

	/**
	 * @var string
	 */
	protected $homepage = 'http://play0ad.com';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://downloads.sourceforge.net/project/zero-ad/releases/0ad-0.0.14-alpha-osx64.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = '0ad';

	/**
	 * @var string
	 */
	protected $appName = '0ad.app';
}
