<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * googlechrome class
 */
class googlechrome extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'Google Chrome';

	/**
	 * @var string
	 */
	protected $homepage = 'https://www.google.com/intl/en/chrome/browser/';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'https://dl.google.com/chrome/mac/stable/GGRO/googlechrome.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'Google Chrome';

	/**
	 * @var string
	 */
	protected $appName = 'Google Chrome.app';
}
