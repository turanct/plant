<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * musescore class
 */
class musescore extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'MuseScore';

	/**
	 * @var string
	 */
	protected $homepage = 'http://musescore.org';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://downloads.sourceforge.net/project/mscore/mscore/MuseScore-1.3/MuseScore-1.3.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'MuseScore-1.3';

	/**
	 * @var string
	 */
	protected $appName = 'MuseScore.app';
}
