<?php
namespace Seeds;


use Plant\SeedDmgPkg as Seed;

/**
 * gpgtools class
 */
class gpgtools extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'GPGTools';

	/**
	 * @var string
	 */
	protected $homepage = 'https://gpgtools.org';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'https://releases.gpgtools.org/GPG%20Suite%20-%202013.08.06.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'GPG Suite';

	/**
	 * @var string
	 */
	protected $appName = 'Install.pkg';
}
