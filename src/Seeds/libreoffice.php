<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * libreoffice class
 */
class libreoffice extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'LibreOffice';

	/**
	 * @var string
	 */
	protected $homepage = 'https://www.libreoffice.org';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://download.documentfoundation.org/libreoffice/stable/4.1.3/mac/x86/LibreOffice_4.1.3_MacOS_x86.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'LibreOffice';

	/**
	 * @var string
	 */
	protected $appName = 'LibreOffice.app';
}
