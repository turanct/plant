<?php
namespace Seeds;


use Plant\SeedZipApp as Seed;

/**
 * bean class
 */
class bean extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'Bean';

	/**
	 * @var string
	 */
	protected $homepage = 'http://www.bean-osx.com';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://www.bean-osx.com/releases/Bean-Install.zip';

	/**
	 * @var string
	 */
	protected $appName = 'Bean-Install/Bean.app';
}
