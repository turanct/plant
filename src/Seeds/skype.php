<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * skype class
 */
class skype extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'Skype';

	/**
	 * @var string
	 */
	protected $homepage = 'https://www.skype.com/';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://www.skype.com/go/getskype-macosx.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'Skype';

	/**
	 * @var string
	 */
	protected $appName = 'Skype.app';
}
