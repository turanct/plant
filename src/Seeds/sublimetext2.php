<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * sublimetext2 class
 */
class sublimetext2 extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'Sublime Text 2';

	/**
	 * @var string
	 */
	protected $homepage = 'http://www.sublimetext.com';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://c758482.r82.cf2.rackcdn.com/Sublime%20Text%202.0.2.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'Sublime Text 2';

	/**
	 * @var string
	 */
	protected $appName = 'Sublime Text 2.app';
}
