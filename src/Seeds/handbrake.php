<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * handbrake class
 */
class handbrake extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'HandBrake';

	/**
	 * @var string
	 */
	protected $homepage = 'http://handbrake.fr';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://downloads.sourceforge.net/project/handbrake/0.9.9/HandBrake-0.9.9-MacOSX.6_GUI_x86_64.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'HandBrake-0.9.9-MacOSX.6_GUI_x86_64';

	/**
	 * @var string
	 */
	protected $appName = 'HandBrake.app';
}
