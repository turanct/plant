<?php
namespace Seeds;


use Plant\SeedDmgPkg as Seed;

/**
 * vagrant class
 */
class vagrant extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'Vagrant';

	/**
	 * @var string
	 */
	protected $homepage = 'http://www.vagrantup.com/';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://files.vagrantup.com/packages/0ac2a87388419b989c3c0d0318cc97df3b0ed27d/Vagrant-1.3.4.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'Vagrant';

	/**
	 * @var string
	 */
	protected $appName = 'Vagrant.pkg';
}
