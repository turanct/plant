<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * armagetronad class
 */
class armagetronad extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'Armagetron Advanced';

	/**
	 * @var string
	 */
	protected $homepage = 'http://armagetronad.org';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://downloads.sourceforge.net/project/armagetronad/stable/0.2.8.3.2/armagetronad-0.2.8.3.2.macosx-universal.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'Armagetron Advanced';

	/**
	 * @var string
	 */
	protected $appName = 'Armagetron Advanced.app';
}
