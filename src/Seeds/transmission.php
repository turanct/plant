<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * transmission class
 */
class transmission extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'transmission';

	/**
	 * @var string
	 */
	protected $homepage = 'http://www.transmissionbt.com';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://download.transmissionbt.com/files/Transmission-2.82.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'Transmission';

	/**
	 * @var string
	 */
	protected $appName = 'Transmission.app';
}
