<?php
namespace Seeds;


use Plant\SeedZipApp as Seed;

/**
 * openarena class
 */
class openarena extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'OpenArena';

	/**
	 * @var string
	 */
	protected $homepage = 'http://openarena.ws';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://openarena.ws/request.php?4';

	/**
	 * @var string
	 */
	protected $appName = 'openarena-0.8.8';
}
