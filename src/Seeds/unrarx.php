<?php
namespace Seeds;


use Plant\SeedZipApp as Seed;

/**
 * unrarx class
 */
class unrarx extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'UnRarX';

	/**
	 * @var string
	 */
	protected $homepage = 'http://unrarx.com/';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://www.unrarx.com/files/UnRarX_2.2.zip';

	/**
	 * @var string
	 */
	protected $appName = 'UnRarX.app';
}
