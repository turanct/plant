<?php
namespace Seeds;


use Plant\SeedZipApp as Seed;

/**
 * mou class
 */
class mou extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'Mou';

	/**
	 * @var string
	 */
	protected $homepage = 'http://mouapp.com';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://mouapp.com/download/Mou_0.6.6.zip';

	/**
	 * @var string
	 */
	protected $appName = 'Mou.app';
}
