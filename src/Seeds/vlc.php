<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * vlc class
 */
class vlc extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'VLC Media Player';

	/**
	 * @var string
	 */
	protected $homepage = 'http://www.videolan.org/';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://ftp.belnet.be/videolan/vlc/2.0.8/macosx/vlc-2.0.8-intel64.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'vlc-2.0.8';

	/**
	 * @var string
	 */
	protected $appName = 'VLC.app';
}
