<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * gimp class
 */
class gimp extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'GIMP';

	/**
	 * @var string
	 */
	protected $homepage = 'http://www.gimp.org';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'ftp://ftp.gimp.org/pub/gimp/v2.8/osx/gimp-2.8.4-nopython-dmg-1.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'GIMP';

	/**
	 * @var string
	 */
	protected $appName = 'GIMP.app';
}
