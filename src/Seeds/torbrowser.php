<?php
namespace Seeds;


use Plant\SeedZipApp as Seed;

/**
 * torbrowser class
 */
class torbrowser extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'TorBrowser';

	/**
	 * @var string
	 */
	protected $homepage = 'https://www.torproject.org';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'https://www.torproject.org/dist/torbrowser/osx/TorBrowser-2.3.25-13-osx-x86_64-en-US.zip';

	/**
	 * @var string
	 */
	protected $appName = 'TorBrowser_en-US.app';
}
