<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * Firefox class
 */
class firefox extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'Firefox';

	/**
	 * @var string
	 */
	protected $homepage = 'https://getfirefox.com';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'https://download.mozilla.org/?product=firefox-latest&os=osx&lang=en-US';

	/**
	 * @var string
	 */
	protected $volumeName = 'Firefox';

	/**
	 * @var string
	 */
	protected $appName = 'Firefox.app';
}
