<?php
namespace Seeds;


use Plant\SeedZipApp as Seed;

/**
 * tinydrop class
 */
class tinydrop extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'TinyDrop';

	/**
	 * @var string
	 */
	protected $homepage = 'https://github.com/turanct/TinyDrop';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'https://github.com/downloads/turanct/TinyDrop/TinyDrop-0.0.1.zip';

	/**
	 * @var string
	 */
	protected $appName = 'TinyDrop.app';
}
