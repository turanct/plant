<?php
namespace Seeds;


use Plant\SeedZipPrefpane as Seed;

/**
 * apptrap class
 */
class apptrap extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'AppTrap';

	/**
	 * @var string
	 */
	protected $homepage = 'http://onnati.net/apptrap/';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://onnati.net/apptrap/download/AppTrap1-1-7.zip';

	/**
	 * @var string
	 */
	protected $appName = 'AppTrap.prefPane';
}
