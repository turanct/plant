<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * audacity class
 */
class audacity extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'Audacity';

	/**
	 * @var string
	 */
	protected $homepage = 'http://audacity.sourceforge.net/';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://audacity.googlecode.com/files/audacity-macosx-ub-2.0.4.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'Audacity 2.0.4';

	/**
	 * @var string
	 */
	protected $appName = 'Audacity';
}
