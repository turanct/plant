<?php
namespace Seeds;


use Plant\SeedDmgPkg as Seed;

/**
 * virtualbox class
 */
class virtualbox extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'VirtualBox';

	/**
	 * @var string
	 */
	protected $homepage = 'https://www.virtualbox.org';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://download.virtualbox.org/virtualbox/4.2.18/VirtualBox-4.2.18-88780-OSX.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'VirtualBox';

	/**
	 * @var string
	 */
	protected $appName = 'VirtualBox.pkg';
}
