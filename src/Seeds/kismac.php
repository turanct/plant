<?php
namespace Seeds;


use Plant\SeedDmgApp as Seed;

/**
 * kismac class
 */
class kismac extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'KisMAC';

	/**
	 * @var string
	 */
	protected $homepage = 'http://kismac-ng.org';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://update.kismacmirror.com/binaries/KisMAC-0.3.3.dmg';

	/**
	 * @var string
	 */
	protected $volumeName = 'KisMAC';

	/**
	 * @var string
	 */
	protected $appName = 'KisMAC.app';
}
