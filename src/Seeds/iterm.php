<?php
namespace Seeds;


use Plant\SeedZipApp as Seed;

/**
 * iterm class
 */
class iterm extends Seed {
	/**
	 * @var string
	 */
	protected $name = 'iTerm2';

	/**
	 * @var string
	 */
	protected $homepage = 'http://www.iterm2.com';

	/**
	 * @var string
	 */
	protected $downloadUrl = 'http://www.iterm2.com/downloads/stable/iTerm2_v1_0_0.zip';

	/**
	 * @var string
	 */
	protected $appName = 'iTerm.app';
}
