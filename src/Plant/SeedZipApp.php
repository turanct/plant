<?php
namespace Plant;


/**
 * SeedZipApp class
 */
class SeedZipApp extends Seed {
	// --------------------------------------------------------- Variables ---------------------------------------------------------
	/**
	 * @var string
	 */
	protected $volumeName;

	/**
	 * @var string
	 */
	protected $appName;


	// ---------------------------------------------------------- Methods ----------------------------------------------------------
	/**
	 * Check method
	 */
	public function checkInstalled() {
		// Is the $appName already in /Applications?
		return file_exists('/Applications/'.basename($this->appName));
	}


	/**
	 * Install method
	 */
	public function install($filename) {
		// Mount
		passthru('PLANT_PWD=`pwd` && cd '.$this->app['settings.tmpDir'].' && open -W -n -g -a "Archive Utility" '.$filename.' && cd $PLANT_PWD');

		// Copy to /Applications
		passthru('sudo mv "'.$this->app['settings.tmpDir'].'/'.$this->appName.'" /Applications');
	}


	/**
	 * Remove method
	 */
	public function remove() {
		// Move to ~/.Trash
		passthru('PLANT_TRASH=`dirname ~/.Trash/bla` && sudo mv "/Applications/'.basename($this->appName).'" "$PLANT_TRASH/'.basename($this->appName, '.app').' '.date('Y-m-d-H-i').'.app"');
	}
}
