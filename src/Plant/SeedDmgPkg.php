<?php
namespace Plant;


/**
 * SeedDmg class
 */
class SeedDmgPkg extends Seed {
	// --------------------------------------------------------- Variables ---------------------------------------------------------
	/**
	 * @var string
	 */
	protected $volumeName;

	/**
	 * @var string
	 */
	protected $appName;


	// ---------------------------------------------------------- Methods ----------------------------------------------------------
	/**
	 * Install method
	 */
	public function install($filename) {
		// Mount
		passthru('hdiutil mount '.$filename);

		// Install
		passthru('sudo installer -package "/Volumes/'.$this->volumeName.'/'.$this->appName.'" -target "/Volumes/Macintosh HD"');

		// Unmount
		passthru('hdiutil unmount "/Volumes/'.$this->volumeName.'"');
	}
}
