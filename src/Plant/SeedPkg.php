<?php
namespace Plant;


/**
 * SeedPkg class
 */
class SeedPkg extends Seed {
	// --------------------------------------------------------- Variables ---------------------------------------------------------



	// ---------------------------------------------------------- Methods ----------------------------------------------------------
	/**
	 * Install method
	 */
	public function install($filename) {
		// Install
		passthru('sudo installer -package '.$filename.' -target "/Volumes/Macintosh HD"');
	}
}
