<?php
namespace Plant;


/**
 * SeedZipPrefpane class
 */
class SeedZipPrefpane extends Seed {
	// --------------------------------------------------------- Variables ---------------------------------------------------------
	/**
	 * @var string
	 */
	protected $volumeName;

	/**
	 * @var string
	 */
	protected $appName;


	// ---------------------------------------------------------- Methods ----------------------------------------------------------
	/**
	 * Check method
	 */
	public function checkInstalled() {
		// Is the $appName already in any of the PreferencePanes library dirs?
		return (file_exists(getenv('HOME').'/Library/PreferencePanes/'.$this->appName) || file_exists('/Library/PreferencePanes/'.$this->appName));
	}


	/**
	 * Install method
	 */
	public function install($filename) {
		// Mount
		passthru('PLANT_PWD=`pwd` && cd '.$this->app['settings.tmpDir'].' && open -W -n -g -a "Archive Utility" '.$filename.' && cd $PLANT_PWD');

		// Install the prefPane by opening it
		passthru('open -W "'.$this->app['settings.tmpDir'].'/'.$this->appName.'"');
	}


	/**
	 * Remove method
	 */
	public function remove() {
		if (file_exists('~/Library/PreferencePanes/'.$this->appName)) {
			// Move to ~/.Trash
			passthru('PLANT_TRASH=`dirname ~/.Trash/bla` && sudo mv "$HOME/Library/PreferencePanes/'.$this->appName.'" "$PLANT_TRASH/'.basename($this->appName, '.prefPane').' '.date('Y-m-d-H-i').'.prefPane"');
		}
		elseif (file_exists('/Library/PreferencePanes/'.$this->appName)) {
			// Move to ~/.Trash
			passthru('PLANT_TRASH=`dirname ~/.Trash/bla` && sudo mv "/Library/PreferencePanes/'.$this->appName.'" "$PLANT_TRASH/'.basename($this->appName, '.prefPane').' '.date('Y-m-d-H-i').'.prefPane"');
		}
	}
}
