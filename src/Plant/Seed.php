<?php
namespace Plant;


/**
 * Seed class
 */
class Seed {
	// --------------------------------------------------------- Variables ---------------------------------------------------------
	/**
	 * @var Pimple
	 */
	protected $app;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $homepage;

	/**
	 * @var string
	 */
	protected $downloadUrl;


	// ---------------------------------------------------------- Methods ----------------------------------------------------------
	/**
	 * Constructor Method
	 */
	public function __construct($app) {
		$this->app = $app;
	}


	/**
	 * Download method
	 */
	public function download() {
		return $this->app['download']($this->downloadUrl, $this->name);
	}


	/**
	 * Magic getter method
	 */
	public function __get($property) {
		if (isset($this->$property)) {
			return $this->$property;
		}
	}
}
