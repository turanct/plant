<?php
namespace Plant;


/**
 * SeedDmgApp class
 */
class SeedDmgApp extends Seed {
	// --------------------------------------------------------- Variables ---------------------------------------------------------
	/**
	 * @var string
	 */
	protected $volumeName;

	/**
	 * @var string
	 */
	protected $appName;


	// ---------------------------------------------------------- Methods ----------------------------------------------------------
	/**
	 * Check method
	 */
	public function checkInstalled() {
		// Is the $appName already in /Applications?
		return file_exists('/Applications/'.$this->appName);
	}


	/**
	 * Install method
	 */
	public function install($filename) {
		// Mount
		passthru('hdiutil mount '.$filename);

		// Copy to /Applications
		passthru('sudo cp -R "/Volumes/'.$this->volumeName.'/'.$this->appName.'" /Applications');

		// Unmount
		passthru('hdiutil unmount "/Volumes/'.$this->volumeName.'"');
	}


	/**
	 * Remove method
	 */
	public function remove() {
		// Move to ~/.Trash
		passthru('PLANT_TRASH=`dirname ~/.Trash/bla` && sudo mv "/Applications/'.basename($this->appName).'" "$PLANT_TRASH/'.basename($this->appName, '.app').' '.date('Y-m-d-H-i').'.app"');
	}
}
